#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
from flask import Flask
import os
import threading
import subprocess

import numpy as np
import cv2
from PIL import ImageGrab
import sys
from butler_common import *
print(API_TRIGGERS, VOICE_TRIGGERS)

#setup python path for subprocess
env_with_butler_libs = os.environ.copy()
env_with_butler_libs["PYTHONPATH"] = f"{script_dir}"
print(env_with_butler_libs["PYTHONPATH"])

def getIP():
    import socket
    #Thanks: https://stackoverflow.com/a/1267524/5282272
    return (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]
app = Flask(__name__)

current_voice_cmd = ""
screen_color = [0,0,0]




#API_COMMANDS
@app.route("/GUI/<page>")
def main_GUI(page):
    return_HTML = "<div class='buttons'>"
    api_buttons = []
    voice_cmds = []
    
    if page == "api_cmd":
        for command in API_COMMANDS:
            button_name = API_COMMANDS[command]
            api_buttons.append(button_name)
        for button_name in api_buttons:
            button_html = f'<a onclick="loadXMLDoc(\'/{button_name}\')" href="javascript:void(0);">{button_name}</a>'
            #button_html = f'<a href="/{button_name}">{button_name}</a>'
            return_HTML = f"{return_HTML}\n{button_html}<br>"  
    
    if page == "voice_cmd":
        for command in VOICE_COMMANDS:
            button_name = VOICE_COMMANDS[command][0]
            voice_cmds.append(button_name)
        for button_name in voice_cmds:
            button_html = f'<a onclick="loadXMLDoc(\'/voice_cmd/{button_name}\')" href="javascript:void(0);">{button_name}</a>'
            #button_html = f'<a href="/{button_name}">{button_name}</a>'
            return_HTML = f"{return_HTML}\n{button_html}<br>"  

    JS = """
    
    <script type="text/javascript">
    function loadXMLDoc(URL)
    {
            xmlhttp=new XMLHttpRequest();
            xmlhttp.open("GET", URL, false);
            xmlhttp.send();
            var data = xmlhttp.responseText;
    }
    </script>
    """
    return_HTML = f"<body>{JS}{css()}\n{menu_html()}\n{return_HTML}\n</div>\n</body>"
    
    return(return_HTML)
    
    
def css():
    return """
    <style>
        body 
        {
            background-color: black;
        }
        .buttons
        {
            width: 100%;
            float: left;
        }
        
        .buttons a
        {
            background:lightblue;
            border-radius: 30px;
            font-size: 30px;
        }
        
        .menu 
        {
            width: 100%; /* Set a width if you like */
            margin-left: 2px;
            padding-left:30px;
            float: right;
            border: 2px solid black;
            background-color: grey;
            border-radius: 35px;
            overflow:hidden;
        }

        .menu a 
        {
            background-color: gray; /* Grey background color */
            color: black; /* Black text color */
            display: block; /* Make the links appear below each other */
            padding: 5px; /* Add some padding */
            padding-left: 20px;
            font-size: 30px;
            text-decoration: none; /* Remove underline from links */
            float: left;
        }
    </style>"""

def menu_html():
    return """

        <div class="menu">
            <a href="/GUI/welcome">Logout</a>
            <a href="/GUI/api_cmd">Static</a>
            <a href="/GUI/voice_cmd">Voice</a>
            <a href="/GUI/ears">Ears</a>
        </div>"""
    
@app.route("/voice_cmd_update/<voice_cmd>")
def process_voice_cmd_part(voice_cmd):
    global current_voice_cmd
    current_voice_cmd = current_voice_cmd + " " + voice_cmd.strip()
    print(current_voice_cmd)
    process_voice_cmd(current_voice_cmd)
    return(voice_cmd)
    

@app.route("/voice_cmd_reset")
def reset_voice_cmd():
    global current_voice_cmd
    current_voice_cmd = ""
    return("reset")

@app.route("/voice_cmd/<voice_cmd>")
def process_voice_cmd(voice_cmd):
    global current_voice_cmd
    global movie_magic
    #voice_cmd = voice_cmd.replace("'", "\\'")
    
    
    for trigger_word in VOICE_TRIGGERS:
        if trigger_word in voice_cmd:
            print(f"Found {trigger_word}")
            for command_file in VOICE_COMMANDS:
                trigger_words = VOICE_COMMANDS[command_file]
                if trigger_word in trigger_words:
                    current_voice_cmd = ""
                    print(f"Found file to run: {command_file}")
                    subprocess.Popen([command_file], env=env_with_butler_libs)

    
    return(voice_cmd)

@app.route("/<stuff>")
def simple_trigger(stuff):
    for api_trigger in API_TRIGGERS:
        if api_trigger == stuff:
            print(f"Found {api_trigger}")
            for trigger_file in API_COMMANDS:
                trigger_word = API_COMMANDS[trigger_file]
                if api_trigger == trigger_word:
                    print(f"Found API to run {trigger_file}")
                    subprocess.Popen([trigger_file], env=env_with_butler_libs)
    return(stuff)
    

if __name__ == "__main__":
    app.run(host=getIP(),debug=True)


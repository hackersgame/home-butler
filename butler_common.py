#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import glob
import subprocess
import json
from settings import *

script_path = os.path.realpath(os.path.abspath(__file__))
script_dir = os.path.dirname(script_path)

def home_ass_cmd(entity_id, cmd, entity_data=None, api_type="services"):
    entity_type = entity_id.split('.')[0]
    cmd = f"{entity_type}/{cmd}"
    full_cmd = f'curl -k -X POST -H "{API_KEY}" -H "Content-Type: application/json"'
    if entity_data == None:
        full_cmd = full_cmd + ' -d \'{"entity_id": "' + entity_id + '"}\' ' + ass_base_url + f'/api/{api_type}/' + cmd
    else:
        full_cmd = full_cmd + ' -d \'{"entity_id": "' + entity_id + f'", {entity_data}' + '}\' ' + ass_base_url + f'/api/{api_type}/' + cmd
    print(f"running: {entity_id} {cmd}")
    #print(full_cmd)
    os.system(full_cmd)
    

def home_ass_states():
    full_cmd = f'curl -X GET -H "{API_KEY}" -H "Content-Type: application/json"'
    full_cmd = full_cmd + f" {ass_base_url}/api/states"
    print(f"Getting events")
    
    result = subprocess.run(full_cmd, stdout=subprocess.PIPE, shell=True)
    data = json.loads(result.stdout.decode())
    return(data)


def home_ass_data(entity_id):
    full_cmd = f'curl -X GET -H "{API_KEY}" -H "Content-Type: application/json"'
    full_cmd = full_cmd + f" {ass_base_url}/api/states/{entity_id}"
    print(f"Getting data for: {entity_id}")
    
    result = subprocess.run(full_cmd, stdout=subprocess.PIPE, shell=True)
    data = json.loads(result.stdout.decode())
    return(data)
    #os.system(full_cmd)

def say(something):
    full_cmd = f"espeak '{something}'"
    print(f"Running: {full_cmd}")
    
    result = subprocess.run(full_cmd, stdout=subprocess.PIPE, shell=True)
    #data = json.loads(result.stdout.decode())
    #return(data)

def get_api_cmds():
    return_data = {}
    api_files = f"{script_dir}/api_commands/*.py"
    file_names = glob.glob(api_files)
    for file_path in file_names:
        name = file_path.split("/")[-1][:-3]
        return_data[file_path] = name
    return(return_data)
    return()

def get_voice_cmds():
    return_data = {}
    voice_files = f"{script_dir}/voice_commands/*.py"
    file_names = glob.glob(voice_files)
    for file_path in file_names:
        name = file_path.split("/")[-1][:-3]
        names = name.split("_")
        return_data[file_path] = names
    return(return_data)

API_COMMANDS = get_api_cmds()
VOICE_COMMANDS = get_voice_cmds()

VOICE_TRIGGERS = []
for voice_file in VOICE_COMMANDS:
    for trigger_word in VOICE_COMMANDS[voice_file]:
        VOICE_TRIGGERS.append(trigger_word)

API_TRIGGERS = []
for API_file in API_COMMANDS:
    API_TRIGGERS.append(API_COMMANDS[API_file])

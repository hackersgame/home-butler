#!/usr/bin/env python3

import argparse
import os
import queue
import sounddevice as sd
import vosk
import sys
import urllib.parse

script_path = os.path.realpath(os.path.abspath(__file__))
script_dir = os.path.dirname(script_path)
print(script_dir)
os.chdir(script_dir)

q = queue.Queue()

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    #if status:
        #print(status, file=sys.stderr)
    q.put(bytes(indata))

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    '-f', '--filename', type=str, metavar='FILENAME',
    help='audio file to store recording to')
parser.add_argument(
    '-m', '--model', type=str, metavar='MODEL_PATH',
    help='Path to the model')
parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='input device (numeric ID or substring)')
parser.add_argument(
    '-r', '--samplerate', type=int, help='sampling rate')
args = parser.parse_args(remaining)

try:
    if args.model is None:
        args.model = "model"
    if not os.path.exists(args.model):
        print ("Please download a model for your language from https://alphacephei.com/vosk/models")
        print ("and unpack as 'model' in the current folder.")
        parser.exit(0)
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, 'input')
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info['default_samplerate'])

    model = vosk.Model(args.model)

    if args.filename:
        dump_fn = open(args.filename, "wb")
    else:
        dump_fn = None

    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 8000, device=args.device, dtype='int16',
                            channels=1, callback=callback):
            print('#' * 80)
            print('Press Ctrl+C to stop the recording')
            print('#' * 80)

            #rec = vosk.KaldiRecognizer(model, )
            rec = vosk.KaldiRecognizer(model, args.samplerate)
            processed_words = ""
            while True:
                data = q.get()
                ok = rec.AcceptWaveform(data)
                if ok:
                    
                    cmd = rec.Result().split('"')[-2]
                    if cmd.startswith("the"):
                        cmd = cmd[3:].strip()
                    
                    processed_words = ""
                    #print(f"\nCMD: {cmd}\n")
                    #url_cmd = urllib.parse.quote(cmd)
                    cmd = f"curl 'http://10.250.10.133:5000/voice_cmd_reset'"
                    #print(cmd)
                    os.system(cmd)
                    #print(rec.Result())
                else:
                    new_word = ""
                    part = rec.PartialResult().split('"')[-2]
                    if part.startswith("the"):
                        part = part[3:].strip()
                    if processed_words in part:
                        if processed_words != "":
                            new_word = part.split(processed_words)[-1]
                        else:
                            new_word = part
                    elif len(processed_words.split(" ")) > 1:
                        num_words_to_keep = len(part.split(" ")) - len(processed_words.split(" "))
                        if num_words_to_keep <= 1:
                            new_word = " ".join(part.split(" ")[:num_words_to_keep*-1])
                    else:
                        new_word = part       
                    if new_word != "":
                        print(f"\rNew: {new_word}")
                        new_word = new_word.strip()
                        processed_words = part
                        url_cmd = urllib.parse.quote(new_word)
                        cmd = f"curl 'http://10.250.10.133:5000/voice_cmd_update/{url_cmd}'"
                        #print(cmd)
                        os.system(cmd)

                    #print(f"Part: {part}",end='\r')
                #if dump_fn is not None:
                    #dump_fn.write(data)

except KeyboardInterrupt:
    print('\nDone')
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))

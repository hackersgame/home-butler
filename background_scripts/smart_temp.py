#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import time
import datetime
import os

#import butler stuff
from butler_common import *

ideal_temp = 69
temp_buffer = 2

while True:
    #only run every X
    time.sleep(10)
    
    #Get data from home ass
    temp_in_room = float(home_ass_data("sensor.sensor_brick_temperature")['state'])
    weather_outside = home_ass_data("weather.home")["attributes"]
    temp_outside = float(weather_outside["temperature"])
    #bedroom_window = home_ass_data("binary_sensor.window_ias_zone")['state']
    
    #Debug info: 
    print(f"temp_in_room: {temp_in_room}")
    print(f"temp_outside: {temp_outside}")
    
    #Check if window fan should be on
    if temp_in_room + temp_buffer > ideal_temp:
        if temp_outside < temp_in_room:
            print("Window fan should be on")
            #check fan state
            fan_state = home_ass_data("switch.window_fan_on_off")["state"]
            if fan_state == "on":
                print("Good thing it already is... :)")
            else:
                print("Powering on fan.")
                home_ass_cmd("switch.window_fan_on_off", "turn_on")
            print(fan_state)
            continue
    print("Window fan should be off")
    fan_state = home_ass_data("switch.window_fan_on_off")["state"]
    if fan_state == "on":
        print("Powering fan off.")
        home_ass_cmd("switch.window_fan_on_off", "turn_off")
    else:
        print("Good thing it already is... :)")
    

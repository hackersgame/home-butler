#!/usr/bin/python3


#GPL3 based on: https://github.com/jb-0001/object-motion-detection

import os
import cv2



video = cv2.VideoCapture("rtsp://10.250.10.15:8554/unicast")

"""
while(True):
    ret, frame = video.read()
    cv2.imshow('VIDEO', frame)
    cv2.waitKey(1)
"""


ret, frame1 = video.read()
ret, frame2 = video.read()

lastPose = -1
num_move_frames = 0
while video.isOpened():
    difference = cv2.absdiff(frame1, frame2)
    grayscale = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(grayscale, (5, 5), 0)
    _, threshold = cv2.threshold(blur, 35, 255, cv2.THRESH_BINARY)
    dilated = cv2.dilate(threshold, None, iterations=2)
    contours, _ = cv2.findContours(
    dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)
        if cv2.contourArea(contour) < 950:
            continue
        cv2.drawContours(frame1, contours, -1, (0, 255, 0), 1)
        cv2.rectangle(frame1, (x, y), (x+w, y+h), (120, 0, 150), 2)
        lastPose = x
    if contours != []:
        num_move_frames = num_move_frames + 1
        #print(f"Updating lastPose: {lastPose}")
    if contours == []:
        if lastPose != -1:
            print(f"End of movement: {lastPose}, amount: {num_move_frames}")
            if num_move_frames < 35:
                print(f"Motion to fast: {num_move_frames}")
            elif num_move_frames > 90:
                print(f"Motion too slow: {num_move_frames}")
            elif lastPose < 200:
                print("Welcome to the office")
                os.system(f"curl 10.250.10.133:5000/person_entered_office")
            else:
                print("Bye for now")
                os.system(f"curl 10.250.10.133:5000/person_exited_office")
            lastPose = -1
            num_move_frames = 0
    #cv2.imshow("feed", frame1)
    cv2.waitKey(1)
    frame1 = frame2
    ret, frame2 = video.read()




cv2.destroyAllWindows()
video.release()



#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

ass_base_url = "http://<IP>:<Port>"
#Example:
#ass_base_url = "http://192.168.1.11:8123"

API_KEY = "<Home Ass API KEY>"
#Example:
#API_KEY = "Authorization: Bearer blah blah blah"

OCTOPRINT_API_KEY = "<Octoprint API KEY>"
#Example:
#OCTOPRINT_API_KEY = "X-Api-Key: blah blah blah"

TOP_LIGHTS = []
#Example:
#TOP_LIGHTS = ["light.sengled_e11_n1ea_aaa_level_light_color_on_off",
#              "light.sengled_e11_n1ea_bbb_level_light_color_on_off",
#              "light.sengled_e11_n1ea_ccc_level_light_color_on_off"]

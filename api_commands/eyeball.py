#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
from butler_common import *
print("eyeball")
cmd = "curl -k4 --request POST -H '" + OCTOPRINT_API_KEY + "' -H 'Content-Type: application/json' --data '{\"command\":\"select\",\"print\":true}' http://10.250.10.207/api/files/local/CFFFP_eyeball_v6-GPL_Post.gcode"
os.system(cmd)
cmd = "espeak 'Printing more eyeballs'"
os.system(cmd)

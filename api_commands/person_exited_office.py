#!/usr/bin/python3
import os
from butler_common import *
OFFICE_PERSON_FILE = "/tmp/num_people_in_office.txt"

def set_office_count(new_state):
    with open(OFFICE_PERSON_FILE, "w+") as fh:
        fh.write(str(new_state))

def get_office_count():
    with open(OFFICE_PERSON_FILE, "r+") as fh:
        return(int(fh.readlines()[0].strip()))

if not os.path.isfile(OFFICE_PERSON_FILE):
    set_office_count(0)

num_in_office_before =  get_office_count()
new_count = num_in_office_before - 1
if new_count < 0:
    new_count = 0

set_office_count(new_count)
if new_count == 0:
    cmd = f"espeak 'Powering off office.'"
    for light in OFFICE:
        #home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": [255,0,0]')
        #set brightness and color_temp
        home_ass_cmd(light,  "turn_off")
else:
    cmd = f"espeak 'Office now has {new_count} people'"
os.system(cmd)

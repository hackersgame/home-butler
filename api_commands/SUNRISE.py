#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import time

#import butler stuff
from butler_common import *
#home_ass_cmd("switch.red_alert_on_off", "turn_off")


sun_rise_lights = ALL_LIGHTS 

total_time = 254
colors = [[196,118,130], [241,146,97], [235,181,83], [230,210,101], [238,241,213], [238,244,254]]

def mix_it(color1, color2, amount):
    red1 = color1[0]
    red2 = color2[0]
    
    green1 = color1[1]
    green2 = color2[1]
    
    blue1 = color1[2]
    blue2 = color2[2]
    
    color1_amount = 1-amount
    color2_amount = amount
    
    new_red = (red1*color1_amount) + (red2*color2_amount)
    new_green = (green1*color1_amount) + (green2*color2_amount)
    new_blue = (blue1*color1_amount) + (blue2*color2_amount)
    return([int(new_red), int(new_green), int(new_blue)])
    
def change_lights(color, brightness_level):
    for light in sun_rise_lights:
        #change color to red
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": ' + str(color))

last_color = [196,118,130]
with open("/tmp/light-blocking-SUNRISE", "w+") as fh:
    pass
#change_lights([0,0,0], 0)
#Start at 40 because my lights strips don't light low light levels
for current_time_index in range(40,total_time):
    if not os.path.isfile("/tmp/light-blocking-SUNRISE"):
        exit()
    time.sleep(.7)
    print(current_time_index)
    color_index = (len(colors)/total_time) * current_time_index
    next_color = int(color_index) + 1
    last_color_index = int(color_index)
    if colors[last_color_index] != last_color:
        last_color = colors[last_color_index]
    next_color = colors[next_color]
    print(f"MIX: {last_color} with {next_color} %{color_index-last_color_index}")
    color_to_be = mix_it(last_color, next_color, color_index-last_color_index)
    change_lights(color_to_be, current_time_index)
    print(color_to_be)
    print(color_index)
    #for color in colors:
        #change_lights(color, brightness_level)
        #brightness_level = brightness_level + brightness_to_add

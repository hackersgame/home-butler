#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import time
import datetime
import os
import ast

#import butler stuff
from butler_common import *


#temp_in_room = home_ass_data("sensor.sensor_brick_temperature")['state']
#bedroom_window = home_ass_data("binary_sensor.window_ias_zone")['state']
cube_data = home_ass_data("input_text.cube1_data")
json_data = cube_data['state'].strip('"')
cube_event_data = ast.literal_eval(json_data)


def get_current_target():
    if not os.path.isfile("/tmp/cube1_target"):
        return None
    with open("/tmp/cube1_target") as fh:
        file_data = fh.readlines()
        for line in file_data:
            if line.startswith("target:"):
                return(line.split(":")[-1])
 
def set_target(new_target):
    old_face = get_current_face()
    with open("/tmp/cube1_target", "w+") as fh:
        fh.write(f"target:{new_target}")
 
def get_current_face():
    if not os.path.isfile("/tmp/cube_now"):
        return None
    with open("/tmp/cube_now") as fh:
        file_data = fh.readlines()
        for line in file_data:
            if line.startswith("face:"):
                return(line.split(":")[-1])

def get_previous_face():
    if not os.path.isfile("/tmp/cube_was"):
        return None
    with open("/tmp/cube_was") as fh:
        file_data = fh.readlines()
        line = file_data[-1]
        if line.startswith("face:"):
            return(line.split(":")[-1])

def write_new_face(new_face):
    old_face = get_current_face()
    with open("/tmp/cube_now", "w+") as fh:
        fh.write(f"face:{new_face}")
    if old_face:
        with open("/tmp/cube_was", "a+") as fh:
            fh.write(f"face:{old_face}\n")

def get_color(target):
    data = home_ass_data(target)
    #'brightness': 254, 'color_temp': 185, 'hs_color': [27.673, 14.004], 'rgb_color'
    if data['state'] == "off":
        #       R,G,B,brightness
        return([0,0,0,0])
    if "rgb_color" in data["attributes"]:
        color_data = data["attributes"]["rgb_color"]
        color_data.append(data["attributes"]["brightness"])
        return(color_data)
    else:
        print(data.keys())

def change_lights(color, lights):
    for light in lights:
        print(f"Change {light} to {color}")

        #change color to red
        #home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": ' + str(color))
def change_brightness(brightness_level, lights):
    for light in lights:
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        #home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": ' + str(color))

def power_off_lights(lights):
    for light in lights:
        #change color to red
        #home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        home_ass_cmd(light,  "turn_off")


def power_on_lights(lights):
    for light in lights:
        #change color to red
        #home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": 254, "color_temp": 185')


#upate event data
if "activated_face" in cube_event_data:
    write_new_face(cube_event_data["activated_face"])
    #if cube_event_data["activated_face"] == 1:
        #say("Rotate to change target")
        #print("Trun to change target")

#Setup things to control
targets = {"All Lights": ALL_LIGHTS, "Top Lights": TOP_LIGHTS, "Office": OFFICE}

#Cube shake
if "value"in cube_event_data:
    if cube_event_data['value'] == 0.0:
        target = get_current_target()
        say(f"{target} selected.")
        #trigger rotate event
        cube_event_data["flip_degrees"] = 90

#Cube rotated
if "relative_degrees" in cube_event_data:
    turn = cube_event_data["relative_degrees"]
    current_face = get_current_face()
    #process target change on face one
    if current_face == "1.0":
        print("yep")
        old_target = get_current_target()
        if not old_target:
            old_target = list(targets.keys())[0]
        old_target_index = list(targets.keys()).index(old_target)
        if turn > 0:
            new_target_index = old_target_index + 1
        else:
            new_target_index = old_target_index - 1
        if new_target_index >= len(list(targets.keys())):
            new_target_index = 0
        print(f"New index: {new_target_index}")
        new_target = list(targets.keys())[new_target_index]
        set_target(new_target)
        say(f"{new_target} selected")
    elif current_face == "4.0":
        target_name = get_current_target()
        target_ids = targets[target_name]
        if turn > 0:
            power_on_lights(target_ids)
            say(f"{target_name} set to max power")
        else:
            say(f"Powering off {target_name}")
            power_off_lights(target_ids)
    else:
        target_name = get_current_target()
        print(target_name)
        #target_name = list(targets.keys())[target_index]
        print(target_name)
        target_ids = targets[target_name]
        if current_face == "2.0":
            current_color = get_color(target_ids[0])
            current_red = current_color[0]
            new_red = current_red + int(turn)
            if new_red > 255:
                new_red = 255
            elif new_red < 0:
                new_red = 0
            new_color = current_color[:-1]
            new_color[0] = new_red
            print(f"New red {new_color}: old {current_color}")
            say(f"Red set to {new_red}")
            change_lights(new_color, target_ids)
        if current_face == "3.0":
            print("Green")
            current_color = get_color(target_ids[0])
            current_green = current_color[1]
            new_green = current_green + int(turn)
            if new_green > 255:
                new_green = 255
            elif new_green < 0:
                new_green = 0
            new_color = current_color[:-1]
            new_color[1] = new_green
            print(f"New green {new_color}: old {current_color}")
            change_lights(new_color, target_ids)
            say(f"Green set to {new_green}")
        if current_face == "5.0":
            print("Blue")
            current_color = get_color(target_ids[0])
            current_blue = current_color[2]
            new_blue = current_blue + int(turn)
            if new_blue > 255:
                new_blue = 255
            elif new_blue < 0:
                new_blue = 0
            new_color = current_color[:-1]
            new_color[2] = new_blue
            print(f"New blue {new_color}: old {current_color}")
            change_lights(new_color, target_ids)
            say(f"Blue set to {new_blue}")
        if current_face == "6.0":
            print("Brightness")
            current_color = get_color(target_ids[0])
            current_brightness = current_color[-1]
            new_brightness = current_brightness + int(turn)
            if new_brightness > 255:
                new_brightness = 255
            elif new_brightness < 0:
                new_brightness = 0
            change_brightness(new_brightness, target_ids)
            print(f"New brightness {new_brightness}: old {current_color}")
            say(f"Brightness set to {new_brightness}")
        
    print(f"Trun: {turn}")

if "flip_degrees" in cube_event_data:
    current_face = get_current_face()
    current_target = get_current_target()
    if current_face == "1.0":
        say("Change target")
    if current_face == "2.0":
        say("Change Red")
    if current_face == "3.0":
        say("Change Green")
    if current_face == "4.0":
        say("Power on or off")
    if current_face == "5.0":
        say("Change Blue")
    if current_face == "6.0":
        say("Change Brightness")
#print(temp_in_room)
#print(bedroom_window)
print(f"Current face: {get_current_face()}")
print(f"Last fase: {get_previous_face()}")
print(f"Raw data used: {cube_event_data}")
#home_ass_cmd("sensor.sensor_brick_temperature", "", api_type="states")

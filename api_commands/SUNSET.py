#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time

#import butler stuff
from butler_common import *
total_time = 254
color = [241,146,97]

def mix_it(color1, color2, amount):
    red1 = color1[0]
    red2 = color2[0]
    
    green1 = color1[1]
    green2 = color2[1]
    
    blue1 = color1[2]
    blue2 = color2[2]
    
    color1_amount = 1-amount
    color2_amount = amount
    
    new_red = (red1*color1_amount) + (red2*color2_amount)
    new_green = (green1*color1_amount) + (green2*color2_amount)
    new_blue = (blue1*color1_amount) + (blue2*color2_amount)
    return([int(new_red), int(new_green), int(new_blue)])
    
def change_lights(color, brightness_level):
    for light in ALL_LIGHTS:
        #change color to red
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": ' + str(color))

def change_brightness(brightness_level):
    for light in ALL_LIGHTS:
        #change color to red
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))

def power_off_lights():
    for light in ALL_LIGHTS:
        #change color to red
        home_ass_cmd(light,  "turn_off")


#Block lights
with open("/tmp/light-blocking-SUNSET", "w+") as fh:
    pass

#Turn on night sky
home_ass_cmd(STAR_CHARGER, "turn_on")

change_lights(color, total_time)
for current_time_index in range(total_time, 0, -2):
    if not os.path.isfile("/tmp/light-blocking-SUNSET"):
        exit()
    time.sleep(.2)
    change_brightness(current_time_index)
    #for color in colors:
        #change_lights(color, brightness_level)
        #brightness_level = brightness_level + brightness_to_add
power_off_lights()
#Turn on night sky
time.sleep(120)
home_ass_cmd(STAR_CHARGER, "turn_off")

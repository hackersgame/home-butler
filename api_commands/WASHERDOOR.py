#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
import time
import datetime
import os

#import butler stuff
from butler_common import *
#home_ass_cmd("switch.red_alert_on_off", "turn_off")


sun_rise_lights = TOP_LIGHTS 
sun_rise_lights.append("light.gledopto_gl_mc_001_deb9a423_level_light_color_on_off")


total_time = 254
colors = [[196,118,130], [241,146,97], [235,181,83], [230,210,101], [238,241,213], [238,244,254]]
WASH_PATH = "/tmp/washing_state"
wash_time_in_min = 42
WASH_TIME = wash_time_in_min * 60


def mix_it(color1, color2, amount):
    red1 = color1[0]
    red2 = color2[0]
    
    green1 = color1[1]
    green2 = color2[1]
    
    blue1 = color1[2]
    blue2 = color2[2]
    
    color1_amount = 1-amount
    color2_amount = amount
    
    new_red = (red1*color1_amount) + (red2*color2_amount)
    new_green = (green1*color1_amount) + (green2*color2_amount)
    new_blue = (blue1*color1_amount) + (blue2*color2_amount)
    return([int(new_red), int(new_green), int(new_blue)])
    
def change_lights(color, brightness_level):
    for light in sun_rise_lights:
        #change color to red
        home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": ' + str(color))
        home_ass_cmd(light,  "turn_on", entity_data='"brightness": ' + str(brightness_level))
        
        #set brightness and color_temp
        #home_ass_cmd(light,  "turn_on", entity_data='"brightness": 254, "color_temp": 185')


def set_washer_state(new_state):
    with open(WASH_PATH, "w+") as fh:
        fh.write(new_state)

def get_washer_state():
    with open(WASH_PATH, "r+") as fh:
        return(fh.readlines()[0])

if not os.path.isfile(WASH_PATH):
    set_washer_state("EMPTY")

msg = ""
old_waser_state = get_washer_state()
if old_waser_state == "LAVA":
    home_ass_cmd("switch.red_alert_on_off", "turn_off")
    set_washer_state("EMPTY")
    msg = "Plus 1000000 kisses to the unloader!"
elif old_waser_state == "EMPTY":
    with open(WASH_PATH, "w+") as fh:
        msg = "Please start the loaded washer"
        #Set time of started load
        fh.write(str(datetime.datetime.now().timestamp()))
else:
    old_waser_state = float(old_waser_state)
    age = datetime.datetime.now().timestamp() - old_waser_state
    print(age)
    if age < WASH_TIME:
        msg = f"Door closed before end of wash. Resetting timer"
        with open(WASH_PATH, "w+") as fh:
            fh.write(str(datetime.datetime.now().timestamp()))
    else:
        msg = "Washer unloaded"
        set_washer_state("EMPTY")
#change_lights(colors[0], 254)
if msg != "":
    cmd = f"espeak '{msg}'"
    print(cmd)
    os.system(cmd)

# Running #

 * Start home_butler.py
 * Run vosk_dump.py to pipe words to home_butler.py

 # GPL3 baby! #

# Testing #

 * PYTHONPATH=$PYTHONPATH:/path/to/install ./scriptname.py

home-butler
Copyright (C) 2022  David Hamner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

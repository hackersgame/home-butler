#!/usr/bin/python3
#home-butler
#Copyright (C) 2022  David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
import time
import os
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import butler stuff
from butler_common import *

home_ass_cmd("switch.red_alert_on_off", "turn_off")
home_ass_cmd(STAR_CHARGER, "turn_off")
#Stop any blocking scripts
os.system("rm /tmp/light-blocking*")
time.sleep(.4)


current_voice_cmd = ""

for light in ALL_LIGHTS:
    if light in TOP_LIGHTS:
        continue
    if light in OFFICE:
        continue
    home_ass_cmd(light,  "turn_off")


for light in TOP_LIGHTS:
    #change color to red
    #home_ass_cmd(light,  "turn_on", entity_data='"rgb_color": [255,0,0]')
    #set brightness and color_temp
    home_ass_cmd(light,  "turn_on", entity_data='"brightness": 254, "color_temp": 185')
